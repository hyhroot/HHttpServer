package com.wxj.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Date;

/**
 * 创建服务器并启动
 * 1、请求
 * 2、响应
 */
public class Server {
    private ServerSocket server;
    public static void main(String[] rags) {
        Server s = new Server();
        s.star();
    }

    //启动
    private void star() {
        try {
            server = new ServerSocket(9999);
            receive();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    //接收客户端请求
    private void receive() {
        try {
            Socket cleint = server.accept();
            //请求
            Request req = new Request(cleint.getInputStream());

            //响应
            Response rep = new Response(cleint.getOutputStream());
            //正文信息
            rep.println("<html><head><title>哈哈哈哈哈</title>");
            rep.println("</head><body>");
            rep.println("好好学习天天向上").println(req.getParameter("uname")).println("欢迎回来");
            rep.println("</body></html>");

            //输出流,推送到客户端
            rep.pushToClient(200);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //停止服务器
    private void stop() {


    }
}
