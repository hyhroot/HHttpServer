package com.wxj.server;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Date;

/**
 * 封装应答信息
 */
public class Response {
    public static final String CRLF = "\r\n";   //回车换行
    public static final String BLANK = " ";     //空格
    //流
    private BufferedWriter bw;

    //正文
    private StringBuilder content;

    //存储头信息
    private StringBuilder headInfo;
    //存储正文长度
    private int len = 0;

    public Response() {
        headInfo = new StringBuilder();
        content = new StringBuilder();
        len = 0;
    }
    public Response(OutputStream os) {
       this();
       bw = new BufferedWriter(new OutputStreamWriter(os));
    }

    /**
     * 构建正文
     */
    public Response print(String info){
        content.append(info);
        len+=info.getBytes().length;
        return this;

    }

    /**
     * 构建正文+回车
     *
     */
    public Response println(String info){
        content.append(info).append(CRLF);
        len+=(CRLF+info).getBytes().length;
        return this;

    }

    /**
     * 构建响应头
     *
     * @param code 状态码
     *             根据状态码不同提示不同的响应信息
     */
    private void createHeadInfo(int code) {
        //1、HTTP协议版本、状态代码、描述
        headInfo.append("HTTP/1.1").append(BLANK).append(code).append(BLANK);
        switch (code) {
            case 200:
                headInfo.append("ok");
                break;
            case 404:
                headInfo.append("NOT FOUND");
                break;
            case 500:
                headInfo.append("SERVER ERROR");
                break;
        }
        headInfo.append(CRLF);
        //2、响应头信息
        headInfo.append("Server  响应头信息  随意填写").append(CRLF);
        headInfo.append("data:时间：").append(new Date()).append(CRLF);
        headInfo.append("Content-Type: text/html;charset=UTF-8").append(CRLF);
        //正文长度:字节长度
        headInfo.append("Content-Length:").append(len).append(CRLF);
        headInfo.append(CRLF);
    }

    //推送到客户端
    void pushToClient(int code) throws IOException {

        createHeadInfo(code);
        //头信息+分隔符
        bw.append(headInfo.toString());
        //正文
        bw.append(content.toString());
        bw.flush();
    }

    //关闭流
    public void clos(){
        CloseUtil.closeIO(bw);
    }

}
